package com.company;
import java.io.Serializable;

public class Vectors implements Serializable{

    public Vectors(int n)
    {
        data=new int [n];
    }
    public int[] getdata()
    {
        return data;
    }
    public void setdata(int n, int i, int[] arr)
    {
        for (int j = 0; j < n; j++) {
            data[j]=arr[i*n+j];
        }
    }
    private int[] data;
}
