package com.company;

import java.io.*;
public class main {

    public static void encode(String inputFileName,String outputFileName){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
            char[] barray = new char[100];
            String line1 = "";

            String line = "";


            int nRead;
            try {
                while ((nRead = reader.read(barray, 0, barray.length)) != -1)

                {
                    long a = 0;
                    for (int i = 0; i < nRead; i++) {
                        line1 += "b";
                        line += barray[i];
                    }

                    System.out.println(line);
                    byte[] byteline = line.getBytes();
                    byte[] byteline1 = line1.getBytes();
                    byte[] result = new byte[line.length()];

                    for (int i = 0; i < byteline.length; i++) {
                        result[i] = (byte) (byteline[i] ^ byteline1[i % byteline1.length]);
                    }
                    String resStr = new String(result, "cp1251");
                    writer.write(resStr, 0, nRead);
                    System.out.println(resStr);


                }
                writer.close();
                reader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void decode(String inputFileName,String outputFileName){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
            char[] barray = new char[100];
            String line1 = "";

            String line = "";


            int nRead;
            try {
                while ((nRead = reader.read(barray, 0, barray.length)) != -1)

                {
                    long a = 0;
                    for (int i = 0; i < nRead; i++) {
                        line1 += "b";
                        line += barray[i];
                    }

                    System.out.println(line);
                    byte[] byteline = line.getBytes();
                    byte[] byteline1 = line1.getBytes();
                    byte[] result = new byte[line.length()];

                    for (int i = 0; i < byteline.length; i++) {
                        result[i] = (byte) (byteline[i] ^ byteline1[i % byteline1.length]);
                    }
                    String resStr = new String(result, "cp1251");
                    writer.write(resStr, 0, nRead);
                    System.out.println(resStr);


                }
                writer.close();
                reader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) throws FileNotFoundException {
        encode("C:\\Users\\User\\IdeaProjects\\yuri-lab-1\\1lab\\src\\com\\company\\input.txt","C:\\Users\\User\\IdeaProjects\\yuri-lab-1\\1lab\\src\\com\\company\\output.txt");
        encode("C:\\Users\\User\\IdeaProjects\\yuri-lab-1\\1lab\\src\\com\\company\\output.txt","C:\\Users\\User\\IdeaProjects\\yuri-lab-1\\1lab\\src\\com\\company\\output2.txt");
        //encode(args[0],args[1]);
        //decode(args[1],args[2]);
    }
}