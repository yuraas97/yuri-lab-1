package com.company;
import java.util.EmptyStackException;
import java.util.Iterator;

public class ArrayStack<T> implements Stack<T> {

    private int top = 0;
    private Object[] stack = new Object[1];


    @Override
    public long getSize() {
        return top;
    }

    @Override
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    public T peek() throws EmptyStackException {

        if (isEmpty()) {
            throw new EmptyStackException();
        }

        return (T) stack[top - 1];
    }


    @Override
    public T push(T item) {
        if (top == stack.length) {
            Object[] newArr = new Object[stack.length * 2];
            System.arraycopy(stack, 0, newArr, 0, top);
            stack = newArr;
        }
        stack[top++] = item;
        return item;
    }


    @Override
    public T pop() throws EmptyStackException {
        if (isEmpty())
            throw new EmptyStackException();
        if (top < stack.length / 4) {
            Object[] newArr = new Object[stack.length / 2];
            System.arraycopy(stack, 0, newArr, 0, top);
            stack = newArr;
        }

        T topElement = (T) stack[--top];
        stack[top] = null;
        return topElement;
    }


    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int current = 0;

            @Override
            public boolean hasNext() {
                return current < top;
            }

            @Override
            public T next() {
                return (T) stack[current++];
            }
        };
    }
}