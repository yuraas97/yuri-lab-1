package com.company;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        int n1=3,m1=3;
        int[] arr1 = {1,2,3,4,5,6,7,8,9};
        Matrix mat1= new Matrix(n1,m1,arr1);
//serialization
        {
            FileOutputStream fileOut = new FileOutputStream("tmp");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(mat1);
            out.close();
            fileOut.close();
        }
//deserialization
        FileInputStream fileIn = new FileInputStream("tmp");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        mat1 = (Matrix) in.readObject();
        in.close();
        fileIn.close();

        int n2=3,m2=3;
        int[] arr2 = {2,5,6,9,4,6,5,3,1};
        Matrix mat2= new Matrix(n2,m2,arr2);
        Matrix m3= new Matrix(n1,m1,arr1);
        try
        {
            m3=Matrix.Trans(mat1);
            m3.Print(m3);
        }
        catch (DimensionException ex)
        {
            System.out.println(ex.getMessage());
        }
        Matrix mat4= new Matrix(n1,m1,arr1);
        try
        {
            mat4=Matrix.Add(mat1,mat2);
            mat4.Print(mat4);
        }
        catch (DimensionException ex)
        {
            System.out.println(ex.getMessage());
        }
        Matrix mat5= new Matrix(n1,m1,arr1);
        try
        {
            mat5=Matrix.Mult(mat1,mat2);
            mat5.Print(mat5);
        }
        catch (DimensionException ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}
