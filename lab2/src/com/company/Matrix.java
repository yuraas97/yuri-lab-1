package com.company;
import java.io.Serializable;

public class Matrix implements Serializable{
    private Vectors[] columns;


    public Matrix(int n, int m, int[] arr) {
        columns= new Vectors[m];
        for(int i=0;i<m;i++) {
            columns[i] = new Vectors(n);
            columns[i].setdata(n,i,arr) ;
        }
    }

    public static Matrix Mult(Matrix m1, Matrix m2) throws DimensionException {
        if (m1.columns.length!= m2.columns[0].getdata().length)
            throw new DimensionException("Умножение матриц таких размерностей невозможно");
        int[] arr = new int[m1.columns[0].getdata().length*m2.columns.length];
        Matrix res= new Matrix(m1.columns[0].getdata().length,m2.columns.length,arr);
        for (int i=0;i<res.columns.length;i++)
            for (int j=0;j<res.columns[0].getdata().length;j++)
            {
                int sum=0;
                for (int k=0;k<res.columns.length;k++)
                    sum+=m1.columns[k].getdata()[j]*m2.columns[i].getdata()[k];
                res.columns[i].getdata()[j]=sum;
            }
        return res;
    }

    public static Matrix Add(Matrix m1, Matrix m2) throws DimensionException{
        if (m1.columns[0].getdata().length!=m2.columns[0].getdata().length ||m1.columns.length!=m2.columns.length)
            throw new DimensionException("Сложение матриц таких размерностей невозможно");
        int[] arr = new int[m1.columns[0].getdata().length*m1.columns.length];
        Matrix res= new Matrix(m1.columns[0].getdata().length,m1.columns.length,arr);
        for (int i=0;i<res.columns.length;i++)
            for (int j=0;j<res.columns[0].getdata().length;j++)
            {
                res.columns[i].getdata()[j]=m1.columns[i].getdata()[j]+m2.columns[i].getdata()[j];
            }
        return res;
    }

    public static Matrix Trans(Matrix m1) throws DimensionException{
        if (m1.columns[0].getdata().length!=m1.columns.length)
            throw new DimensionException("Транспонирование матрицы таких размерностей невозможно");
        int[] arr = new int[m1.columns[0].getdata().length*m1.columns.length];
        Matrix res= new Matrix(m1.columns.length,m1.columns[0].getdata().length,arr);
        for (int i=0;i<m1.columns[0].getdata().length;i++)
            for (int j=0;j<m1.columns.length;j++)
            {
                res.columns[i].getdata()[j]=m1.columns[j].getdata()[i];
            }
        return res;

    }
    public static void Print(Matrix m1)
    {
        for (int i=0;i<m1.columns[0].getdata().length;i++) {
            for (int j = 0; j < m1.columns.length; j++) {
                System.out.print(m1.columns[i].getdata()[j]+" ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

}

class DimensionException extends IndexOutOfBoundsException
{
    public DimensionException(String message)
    {
        super(message);
    }
}
